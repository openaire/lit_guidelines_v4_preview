This repository provides a preview on the application profile for OpenAIRE Guidelines for Literature repository managers v4.

The schema files are located in directory [schema](schema).

The "root" schema is [openaire-v4.xsd](schema/openaire-v4.xsd).

Documentation is located in directory [documentation](documentation) and generated from the schema file(s).

Examples are provided in directory [example](example).

The application profile consists of
* elements from Dublin Core with optional refinement attributes `xml:lang` and `rdf:resource`
* elements from DataCite metadata schema v4 to express granular information on `creator`, `contributor`, `fundingReference`, `relatedIdentifier`, `alternateIdentifier` and `date` (to express embargo periods)
* elements from bibo to express information on bibliographic citation details of journal articles
* new elements to express information on (fulltext) files and version      